variable "region" {
  default = "eu-west-1"
}

variable "vpc_cidr" {
  default = "172.31.0.0/16"
}
variable "private_subnets" {
  default = ["172.31.0.0/24", "172.31.1.0/24"]
}

variable "public_subnets" {
  default = ["172.31.10.0/24", "172.31.11.0/24"]
}

variable "database_subnets" {
  default = ["172.31.20.0/24", "172.31.21.0/24"]
}
variable "sshpubkey_file" {
  default = "~/.ssh/aws_vnova.pub"
}

variable "instance_type" {
  default = "t2.micro"
}

#variable "vpc_id" {
#  default = "vpc-0e4532c0e879612f3"
#}
