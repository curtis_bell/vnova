module "vpc" {
  name    = "foo"
  source  = "terraform-aws-modules/vpc/aws"
  cidr = "${var.vpc_cidr}"

  azs = [#
    "${data.aws_availability_zones.available.names[0]}",
    "${data.aws_availability_zones.available.names[1]}",
  ]

  private_subnets  = "${var.private_subnets}"
  public_subnets   = "${var.public_subnets}"
  database_subnets = "${var.database_subnets}"

  create_database_subnet_group = false

  enable_dns_support   = true
  
  public_subnet_tags = {
      Env = "Dev"
      Author = "Curtis.bell@hotmail.co.uk"
    }

    private_subnet_tags = {
        Env = "Dev"
        Author = "Curtis.bell@hotmail.co.uk"
      }

  #private_subnet_tags = {
  #    Name = "${var.private_subnet_tags}"
  #  }


}
