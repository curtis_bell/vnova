resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = var.instance_type
  subnet_id = module.vpc.public_subnets[0]
  key_name = "bellc"
}

/*
resource "aws_instance" "web" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = var.instance_type
  subnet_id= module.vpc.private_subnets[0]
}

resource "aws_eip" "lb" {
  instance = aws_instance.web.id
  vpc      = true
}

*/




resource "aws_eip" "lb" {
  instance = aws_instance.bastion.id
  vpc      = true
}
